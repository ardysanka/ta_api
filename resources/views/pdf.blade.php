<style>
    #header img {
        width: 100%;
    }
</style>
<div id="header">
    <img src="https://pendaftaran.budiluhur.ac.id/img/Kartu%20Pendaftaran%20Universitas%20Budi%20Luhur_Header.jpg"
        alt="Header Kartu Pendaftaran">
</div>
<strong>
    <p align="center" style="color:#1A3560;">Formulir Pendaftaran</p>
</strong>
<?php 
$nd = explode(" ",$nama);

// echo "Hai ". $nama[0]; 

?>
<p align="left" style="line-height: 110%;">
    Hi! <?php echo $nd[0]; ?> Terimakasih telah memilih Universitas Budi Luhur - Akademi Budi Luhur sebagai tempat untuk
    berkuliah, berikut adalah detail formulir pendaftaran online kamu:
</p>
<table border="0">
    <tr>
        <td width="35%">Nomor Pendaftaran</td>
        <td width="2%">:</td>
        <td width="63%"><?php echo $notest ; ?></td>
    </tr>
    <tr>
        <td>Nama Pendaftaran</td>
        <td>:</td>
        <td><?php echo $nama; ?></td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>:</td>
        <td><?php echo $nmfakultas;?></td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:</td>
        <td><?php echo $nmprodi; ?></td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>:</td>
        <td><?php echo $nmjenjang; ?></td>
    </tr>
    <tr>
        <td>Jenis Daftar</td>
        <td>:</td>
        <td><?php echo $nmjnsdftr; ?></td>
    </tr>
    <tr>
        <td>Username</td>
        <td>:</td>
        <td><?php echo $notest ; ?></td>
    </tr>
    <tr>
        <td>Password</td>
        <td>:</td>
        <td><?php echo $passpl ; ?></td>
    </tr>

</table>
<p align="left" style="line-height: 110%;">
    Salam Budi Luhur,<br>
    <br>
    Jakarta, $now <br>
    Panitia Penerimaan Mahasiswa Baru<br>
    UNIVERSITAS BUDI LUHUR - AKADEMI SEKRETARI BUDI LUHUR<BR>
    <hr>

</p>
<table>
    <thead>
        <tr>
            <th>Langkah Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        <tr>

            <td>
                <ul>
                    <li>Melakukan Pendaftaran Dan Melakukan Pembayaran</li>
                    <li>Melengkapi Berkas Pendaftaran
                        <ol>
                            <li>Fotocopy Raport SMA/sederajat semester 3/4/5</li>
                            <li>Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga</li>
                            <li>Kartu Pendaftaran yang sudah dicetak melalui email</li>
                            <li>Bukti Pembayaran uang pendaftaran</li>

                        </ol>
                    </li>

                </ul>
            </td>

        </tr>
    </tbody>
</table>
<strong>
    <p style="line-height: 110%;    color: #1A3560;">Informasi dan Pertanyaan, Silahkan Hubungi :<br>
        Telp: (021)5853753 - Ext. 285/286/288</p>
</strong>
<div id="header">
    <img src="https://pendaftaran.budiluhur.ac.id/img/Kartu%20Pendaftaran%20Universitas%20Budi%20Luhur_Footer.jpg"
        alt="Header Kartu Pendaftaran">
</div>