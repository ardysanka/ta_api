<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'kodok terbang';
});
$router->post('/login','CCalonmhs@login');

$router->post('/register','CCalonmhs@register');
$router->post('/upload','UserController@upload');
$router->get('/minat','getdata@minat');
$router->post('/email','getdata@resendemail');
$router->get('/grade/{id}','getdata@grade');
$router->get('/prodi','getdata@allprodi');
$router->get('/profile', 'UserController@profil');
$router->put('/profile/update/', 'UserController@update');
$router->get('/logout', 'UserController@logout');
$router->get('/sma','getdata@sma');
$router->group(['prefix' => 'api'], function () use ($router) {
     // Matches "/api/login
    $router->post('login', 'CCalonmhs@login');
});
// $router->post('/minat','');
// $router->post('/user/{id}',['middleware'=>'auth',
//  'uses'=>'CCalonmhs@get_user']);
