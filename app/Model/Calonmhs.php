<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


use Tymon\JWTAuth\Contracts\JWTSubject;


class Calonmhs extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notest','nama',
        'alamat','telpon',
        'nilaitest','grade',
        'user','kd_sma',
        'kd_test','kd_prodi',
        'kd_jen','kd_minat',
        'thajar','tgldftr',
        'thnlulus','email',
        'tgllhr','nmortu',
        'telportu','agama',
        'status','password',
        'klsintern','statuscalon'
    ];

    // public $timestaps = false;
    protected $primaryKey = 'notest';
    public $timestamps = false;


    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }



    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function jenjang(){
        return $this->belongsTo(Jenjang::class,'kd_jen','kd_jen');
    }
    // public function prodi($kd_jen,$kd_prodi){
    //     // return $this->belongsTo(Prodi::class,'kd_prodi','kd_prodi');
        // return $this->belongsTo(Prodi::class,['kd_prodi','kd_jen'],['kd_prodi','kd_jen']);
    //     // return static::where('kd_jen',$kd_jen)->where('kd_prodi',$kd_prodi)->first();
    // }
    public function minat(){
        return $this->belongsTo(Mminat::class,'kd_minat','kd_minat');
    }
    protected $hidden = [
        'password',
    ];
}
