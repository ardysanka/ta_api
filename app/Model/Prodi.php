<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Prodi extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    protected $table = "prodi";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
        
    // ];
    // public $timestaps = false;
    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    // ];
    public function fakultas(){
        return $this->belongsTo(Fakultas::class,'kd_fak','kd_fak');
    }
    public function jenjang(){
        return $this->belongsTo(Jenjang::class,'kd_jen','kd_jen');
    }
    public function minat(){
        return $this->hasMany(Mminat::class, 'kd_prodi', 'kd_prodi');
    }
}


