<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Jenjang extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'jenjang';
    protected $fillable = [
        'kd_jen','nmjen',
        
       
    ];

    // public $timestaps = false;
    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    // ];


        public function prodi(){
            return $this->hasMany(Prodi::class, 'kd_jen', 'kd_jen');
        }
    // public function minat(){
    //     return $this->hasMany('App\Model\MMinat');
    // }
}
