<?php
namespace App\Helper;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class Fungsi
{
    public function generatepass($length = 5)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function sendemail($array = null)
    {

        try {
            // $file_pointer = '/tmp/t_pmb/Panduan_Pembayaran_Biaya_Pendaftaran_Mahasiswa_Baru_Ver1.1.pdf';
            $file_pointer = '/home/ardy/filehost/Panduan_Pembayaran_Biaya_Pendaftaran_Mahasiswa_Baru_Ver1.1.pdf';
            if (file_exists($file_pointer)) {
                
            } else {
                // $xres['status']= "0001";
                // $data['data']=  $enkrip->enkrip(json_encode($xres));
                return false;
            }
            $pdf = PDF::loadView('pdf', $array)->save('/home/ardy/filehost/' . $array['notest'] . '.pdf');
            Mail::send('email', $array, function ($message) use ($array) {
                $message->subject("[NO-REPLY] Pendaftaran");
                $message->from('situsharam@gmail.com', 'Penerimaan Mahasiswa Baru');
                $message->to($array['email'])->attach('/home/ardy/filehost/Panduan_Pembayaran_Biaya_Pendaftaran_Mahasiswa_Baru_Ver1.1.pdf', [
                    'as' => 'panduan pembayarans.pdf',
                    'mime' => 'application/pdf',
                ]);
                $message->to($array['email'])->attach('/home/ardy/filehost/' . $array['notest'] . '.pdf', [
                    'as' => 'Kartu Pendaftaran.pdf',
                    'mime' => 'application/pdf',
                ]);
            });
            $xxxres['mail'] = "000";

        } catch (Exception $e) {
            // return response (['status' => false,'errors' => $e->getMessage()]);
            $xxxres['mail'] = "0001";
        }
        return $xxxres;
    }

}
