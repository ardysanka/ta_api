<?php
namespace App\Http\Controllers;

use App\Helper\Enkrip;
use App\Helper\Fungsi;
use App\Http\Controllers\Controller;
use App\Model\Calonmhs;
use App\Model\Gel;
use App\Model\Mminat;
use App\Model\Prodi;
use App\Model\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Storage;
// use App\Mail\MyEmail;

use Validator;

class CCalonmhs extends Controller
{

    public function register(Request $request)
    {
        $gel = Gel::where('tglmulai', '<=', date('Y-m-d'))->where('tglselesai', '>=', date('Y-m-d'))->first();
        $fungsi = new Fungsi();
        $enkrip = new Enkrip();
        $ld = Calonmhs::where('thajar', $gel['thajar'])->orderBy('notest', 'desc')->first();
        $nt = substr($ld['notest'], 4, 6);
        $td = Transaksi::where('thajar', $gel['thajar'])->orderBy('notrans', 'desc')->first();
        $trans = substr($td['notrans'], 3, 5);
        $k = '2' . substr($gel['thajar'], 2, 2) . $gel['gel'];
        $t = substr($gel['thajar'], 2, 2) . 'O';
        $nodaftar = $k . sprintf("%06s", $nt + 1);
        $notrans = $t . sprintf("%05s", $trans + 1);
        $validator = Validator::make($request->input(), [
            'nama' => 'required|max:255',
            'email' => 'required|max:60|email',
            'telpon' => 'required',
            'jenjang' => 'required',
            'prodi' => 'required',
            'minat' => 'required',
            'user' => 'required',
        ]);

        if ($validator->fails()) {
            $res['status'] = "0001";
            return response($enkrip->enkrip(json_encode($res)));
        }
        // $res = "xx";
        $passpl = $fungsi->generatepass();
        $pass = app('hash')->make($passpl);
        $register = Calonmhs::create([
            'notest' => $nodaftar,
            'nama' => $request->input('nama'),
            'telpon' => $request->input('telpon'),
            'user' => $request->input('user'),
            'kd_prodi' => $request->input('prodi'),
            'kd_jen' => $request->input('jenjang'),
            'kd_minat' => $request->input('minat'),
            'thajar' => $gel['thajar'],
            'tgldftr' => date('Y-m-d'),
            'email' => $request->input('email'),
            'status' => $request->input('status'),
            'password' => $pass,
            'klsintern' => 'I',
            'nilaitest' => '0',
            'statuscalon' => 1,
        ]);
        $batas = date('Y-m-d H:i', strtotime(' + 5 days'));
        $jumbayar = '150000';
        $transaksi = Transaksi::create([
            'notrans' => $notrans,
            'notest' => $nodaftar,
            'tgltrans' => date('Y-m-d H:i'),
            'smt' => 'O',
            'ket' => "Biaya Formulir",
            'jumbayar' => $jumbayar,
            'tglbatas' => $batas,
            'thajar' => $gel['thajar'],
        ]);

        if ($register) {
            $res['status'] = "0000";
            $res['pass'] = $passpl;
            $res['notest'] = $nodaftar;
            $res['batas'] = $batas;
            $res['jumbayar'] = $jumbayar;

            $minat = Mminat::with('jenjang')->where('kd_minat', $request->input('minat'))->first();

            $program = Prodi::with('fakultas')->where('kd_prodi', $minat['kd_prodi'])->where('kd_jen', $minat['kd_jen'])->first();

            $arraymail = [
                'notest' => $nodaftar,
                'nama' => $request->input('nama'),
                'nmprodi' => $program['nmprodi'],
                'nmfakultas' => $program['fakultas']['nmfak'],
                'nmjenjang' => $minat['jenjang']['nmjen'],
                'email' => $request->input('email'),
                'nmjnsdftr' => 'Jalur Umum',
                'passpl' => $passpl,
            ];
            $mail = $fungsi->sendemail($arraymail);
            if ($mail == false) {
                $res['status'] = "0002";
            } else {
                $res['status'] = "0000";
            }
            $res['namaprodi'] = $program['nmprodi'];
            $res['namajenjang'] = $minat['jenjang']['nmjen'];
            $res['namafakultas'] = $program['fakultas']['nmfak'];

        } else {
            $res['status'] = "0003";
        }
        // $xrest
        $xres['data'] = $enkrip->enkrip(json_encode($res));
        return response($xres);
    }

    public function login(Request $request)
    {
        //validate incoming request
        $enkrip = new Enkrip();
        $this->validate($request, [
            'notest' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['notest', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            // return response()->json(['message' => 'Unauthorized'], 401);
            return response($res['status'] == '0002');
        }
        $transfer = Transaksi::where('notest', $request->input('notest'))->first();
        if ($transfer['tglflag'] == null) {
            $bayar = 0;
        } else {
            $bayar = 1;
        }

        $res['data'] = array(
            'user' => $request->input('notest'),
            'bayar' => $bayar,
            'token' => $token,
            'token_type' => 'bearer',

            'expires_in' => Auth::factory()->getTTL() * 60);

        // ]$this->respondWithToken($token);
        $xres['data'] = $enkrip->enkrip(json_encode($res));
        return $xres;
    }
    public function upload(Request $request)
    {
        
        // $enkrip = new Enkrip();
        // $notest = "xxxxx";
        // $validator = Validator::make($request->file(), [
        //     'kk' => 'mimes:jpeg,png,jpg,pdf|max:1024',
        //     'ra' => 'mimes:jpeg,png,jpg,pdf|max:1024',
        //     'br' => 'mimes:jpeg,png,jpg,pdf|max:1024',
        // ]);
        // if ($validator->fails()) {
        //     $res['status'] = "0001";
        //     return response($enkrip->enkrip(json_encode($res)));
        //     // return response($res);
        // } else {
        //     if ($request->hasFile('kk')) {
        //         $data = $request->file('kk')->getClientOriginalName();
        //         $kk = "kk." . $request->file('kk')->getClientOriginalExtension();
        //         $destination = "/home/ardy/filehost/" . $notest;

        //         if (file_exists($destination)) {
        //             $request->file("kk")->move($destination, $kk);
        //         } else {
        //             mkdir($destination, 0777, true);
        //             $request->file("kk")->move($destination, $kk);
        //         }
        //         if (file_exists($destination)) {
        //             $res['status'] = "0000";
        //         }else{
        //             $res['status'] = "0003";
        //         }
        //     }
        //     if ($request->hasFile('ra')) {
        //         $data = $request->file('ra')->getClientOriginalName();
        //         $kk = "ra." . $request->file('ra')->getClientOriginalExtension();
        //         $destination = "/home/ardy/filehost/" . $notest;

        //         if (file_exists($destination)) {
        //             $request->file("ra")->move($destination, $kk);
        //         } else {
        //             mkdir($destination, 0777, true);
        //             $request->file("ra")->move($destination, $kk);
        //         }
        //         if (file_exists($destination)) {
        //             $res['status'] = "0000";
        //         }else{
        //             $res['status'] = "0003";
        //         }
        //     }
        //     if ($request->hasFile('bp')) {
        //         $data = $request->file('bp')->getClientOriginalName();
        //         $kk = "bp." . $request->file('bp')->getClientOriginalExtension();
        //         $destination = "/home/ardy/filehost/" . $notest;

        //         if (file_exists($destination)) {
        //             $request->file("bp")->move($destination, $kk);
        //         } else {
        //             mkdir($destination, 0777, true);
        //             $request->file("bp")->move($destination, $kk);
        //         }
        //         if (file_exists($destination)) {
        //             $res['status'] = "0000";
        //         }else{
        //             $res['status'] = "0003";
        //         }
        //     }
            
            
        //     return response($res);
        // }

        // $data = $request->file('kk')->getClientOriginalName();
        // $kk = "kk.".$request->file('kk')->getClientOriginalExtension();
        // // $kk = "raport.".$request->file('rapor')->getClientOriginalExtension();
        // // $kk = "br.".$request->file('br')->getClientOriginalExtension();
        // // return response($data);
        // $destination = "/tmp/t_pmb/";
        // $request->file("kk")->move($destination, $kk);
        // return response($request->input('nama'));

    }
    protected function Logout($token)
    {

    }
}
