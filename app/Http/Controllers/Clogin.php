<?php
namespace App\Http\Controllers;
use Validator;
use App\Model\Calonmhs;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
class Clogin extends BaseController
{
    private $request;
    public function __construct(Request $request) {
        $this->request = $request;
    }
    protected function jwt(User $user){
        $payload= [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }
    public function authentication(CCalonmhs $user){
        $this->validate($this->request,[
            'notest'=>'required',
            'password'=>'required'
        ]);
        $user = Calonmhs::where('notest',$this->request->input('notest'))->first();

        if(!$user){
            return response()->json([
                'error'=>'Notest does not exist.'
            ],400);
        }else{
            return response()->json([
                'token' => $this->jwt($user)
            ], 200);
        }

    }
}