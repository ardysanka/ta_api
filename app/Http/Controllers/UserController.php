<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use  App\Model\Calonmhs;
use App\Helper\Enkrip;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
class UserController extends Controller
{
     /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   
    /**
     * Get all User.
     *
     * @return Response
     */
    public function profil()
    {
       
        $enkrip = new Enkrip();
        $user = Auth::payLoad()->toArray();
        // dd($user);
        // print_r($user);

        // return response();
        // return response()->json(['code' => 200, 'data' => ['user' => $user]]);
        $data = Calonmhs::where('notest',$user['sub'])->first();
        $xdata['notest']=$data['notest'];
        $xdata['nama']=$data['nama'];
        $xdata['jenkel']=$data['jenkel'];
        $xdata['tmptlhr']=$data['tmptlhr'];
        $xdata['tgllhr']=$data['tgllhr'];
        $xdata['alamat']=$data['alamat'];
        $xdata['kd_sma']=$data['kd_sma'];
        $xdata['kd_minat']=$data['kd_minat'];
        $xdata['kd_prodi']=$data['kd_prodi'];
        $xdata['kd_jen']=$data['kd_jen'];
        $res['data']=$xdata;
        $xres['data'] = $enkrip->enkrip(json_encode($res));
        return response($xres);
        //  return response($user->token());
        // return response("xxx");
    }
    public function update(Request $request)
    {
        // dd($request->all());
        // exit();
        $enkrip = new Enkrip();
        $user = Auth::payLoad()->toArray();
        // dd($user);
        // print_r($user);
        $flag=0;

        // return response();
        // return response()->json(['code' => 200, 'data' => ['user' => $user]]);
        $data = Calonmhs::where('notest',$user['sub'])->first();
        if($data['jenkel']!=$request->input('jenkel')){
            $data->jenkel = $request->input('jenkel');
            $flag=1;
            $xdata['1cek']="1";
        }
        if($data['tmptlhr']!=$request->input('tmptlhr')){
            $data->tmptlhr = $request->input('tmptlhr');
            $flag=1;
            $xdata['2cek']="1";
        }
        if($data['tgllhr']!=$request->input('tgllhr')){
            
            // $data->tgllhr = date($request->input('tgllhr'),"d-m-y");
            $data->tgllhr = Carbon::parse($request->input('tgllhr'))->format('Y-m-d');
            $flag=1;
            $xdata['3cek']="1";
        }
        if($data['alamat']!=$request->input('alamat')){
            $data->alamat = $request->input('alamat');
            $flag=1;
            $xdata['4cek']="1";
        }
        if($data['kd_sma']!=$request->input('kd_sma')){
            $data->kd_sma = $request->input('kd_sma');
            $flag=1;
            $xdata['5cek']="1";
        }
        // if($request->hasFile('k')){
        //     $xdata['kk']="ada";
        // }else{
        //     $xdata['kk']="ga ada";
        // }
        if($flag==1){
            $xdata['cek']="1";
            $data->save();
        }
        $data = Calonmhs::where('notest',$user['sub'])->first();
        $xdata['notest']=$data['notest'];
        $xdata['nama']=$data['nama'];
        $xdata['jenkel']=$data['jenkel'];
        $xdata['tmptlhr']=$data['tmptlhr'];
        $xdata['tgllhr']=$data['tgllhr'];
        $xdata['alamat']=$data['alamat'];
        $xdata['kd_sma']=$data['kd_sma'];
        $res['data']=$xdata;
        $xres['data'] = $enkrip->enkrip(json_encode($res));
        return response($xres);

        
    }
    public function upload(Request $request){
               
        $enkrip = new Enkrip();
        $user = Auth::payLoad()->toArray();
        $notest = $user['sub'];
        $flag = 0;
        $validator = Validator::make($request->file(), [
            'kk' => 'mimes:jpeg,png,jpg,pdf|max:1024',
            'ra' => 'mimes:jpeg,png,jpg,pdf|max:1024',
            'br' => 'mimes:jpeg,png,jpg,pdf|max:1024',
        ]);
        if ($validator->fails()) {
            $res['status'] = "0001";
            return response($enkrip->enkrip(json_encode($res)));
            // return response($res);
        } else {
            if ($request->hasFile('kk')) {
                $data = $request->file('kk')->getClientOriginalName();
                $kk = "kk." . $request->file('kk')->getClientOriginalExtension();
                $destination = "/home/ardy/filehost/" . $notest;

                if (file_exists($destination)) {
                    $request->file("kk")->move($destination, $kk);
                } else {
                    mkdir($destination, 0777, true);
                    $request->file("kk")->move($destination, $kk);
                }
                if (file_exists($destination)) {
                    $res['status'] = "0000";
                }else{
                    $res['status'] = "0003";
                }
                $flag = 1;
            }
            if ($request->hasFile('ra')) {
                $data = $request->file('ra')->getClientOriginalName();
                $kk = "ra." . $request->file('ra')->getClientOriginalExtension();
                $destination = "/home/ardy/filehost/" . $notest;

                if (file_exists($destination)) {
                    $request->file("ra")->move($destination, $kk);
                } else {
                    mkdir($destination, 0777, true);
                    $request->file("ra")->move($destination, $kk);
                }
                if (file_exists($destination)) {
                    $res['status'] = "0000";
                }else{
                    $res['status'] = "0003";
                }
                $flag = 1;
            }
            if ($request->hasFile('bp')) {
                $data = $request->file('bp')->getClientOriginalName();
                $kk = "bp." . $request->file('bp')->getClientOriginalExtension();
                $destination = "/home/ardy/filehost/" . $notest;

                if (file_exists($destination)) {
                    $request->file("bp")->move($destination, $kk);
                } else {
                    mkdir($destination, 0777, true);
                    $request->file("bp")->move($destination, $kk);
                }
                if (file_exists($destination)) {
                    $res['status'] = "0000";
                }else{
                    $res['status'] = "0003";
                }
                $flag = 1;
            }
            
            if ($flag==0) {

            
                $res['status'] = "0005";
            }
            $xres['data'] = $enkrip->enkrip(json_encode($res));
            return response($xres);
            
        }

    }
    public function logout(){
        // $this->jwt->parseToken()->invalidate();
        Auth::parseToken()->invalidate();
        return "xxx";
    }

  

}