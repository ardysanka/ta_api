<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jenjang;
use App\Helper\Enkrip;
class CMinat extends Controller
{
    public function all(){

        $enkrip = new Enkrip();
        $data = Jenjang::all();
        $res['data'] = $enkrip->enkrip($data);
        return response($res);
    }
}