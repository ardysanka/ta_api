<?php
namespace App\Http\Controllers;

use App\Helper\Enkrip;
use App\Helper\Fungsi;
use App\Http\Controllers\Controller;
use App\Model\Calonmhs;
use App\Model\Mminat;
use App\Model\Msma;
use App\Model\Prodi;
use Illuminate\Http\Request;

class getdata extends Controller
{

    public function minat(Request $request)
    {
        $xres = array();
        $xres['data'] = array();
        $dx = array();
        // 0 tidak aktif, != 0 || 1 reguler, 2 karyawan, 3 international
        $cek = Prodi::where('status', '!=', '1')->get();

        if ($request->input('kelas') == 1) {
            $cek = Prodi::where('status', '!=', '0')->get();
            foreach ($cek as $key => $value) {
                array_push($dx, $value['kd_jen'] . $value['kd_prodi']);
            }
            $data = Mminat::with('jenjang', 'prodi.fakultas')->orderBy('kd_prodi', 'ASC')->orderBy('kd_jen', 'ASC')->get();
            foreach ($data as $key => $value) {
                if (!in_array($value['kd_jen'] . $value['kd_prodi'], $dx)) {
                    continue;
                }
                $x['kd_minat'] = $value['kd_minat'];
                $x['nmminat'] = $value['nmminat'];
                $x['kd_jen'] = $value['kd_jen'];
                $x['kd_prodi'] = $value['kd_prodi'];
                $x['nmprodi'] = $value['prodi']['nmprodi'];
                $x['nmjen'] = $value['jenjang']['nmjen'];
                $x['kd_fak'] = $value['prodi']['fakultas']['kd_fak'];
                $x['nmfak'] = $value['prodi']['fakultas']['nmfak'];
                array_push($xres['data'], $x);
            }
        } elseif ($request->input('kelas') == 2) {
            $cek = Prodi::where('status', '=', '2')->get();
            foreach ($cek as $key => $value) {
                array_push($dx, $value['kd_jen'] . $value['kd_prodi']);
            }
            $data = Mminat::with('jenjang', 'prodi.fakultas')->orderBy('kd_prodi', 'ASC')->orderBy('kd_jen', 'ASC')->get();
            foreach ($data as $key => $value) {
                if (!in_array($value['kd_jen'] . $value['kd_prodi'], $dx)) {
                    continue;
                }
                $x['kd_minat'] = $value['kd_minat'];
                $x['nmminat'] = $value['nmminat'];
                $x['kd_jen'] = $value['kd_jen'];
                $x['kd_prodi'] = $value['kd_prodi'];
                $x['nmprodi'] = $value['prodi']['nmprodi'];
                $x['nmjen'] = $value['jenjang']['nmjen'];
                $x['kd_fak'] = $value['prodi']['fakultas']['kd_fak'];
                $x['nmfak'] = $value['prodi']['fakultas']['nmfak'];
                array_push($xres['data'], $x);
            }
        } elseif ($request->input('kelas') == 3) {
            $data = Mminat::where('kd_minat', '3I')->with('jenjang', 'prodi.fakultas')->orderBy('kd_prodi', 'ASC')->orderBy('kd_jen', 'ASC')->get();
            foreach ($data as $key => $value) {
                $x['kd_minat'] = $value['kd_minat'];
                $x['nmminat'] = $value['nmminat'];
                $x['kd_jen'] = $value['kd_jen'];
                $x['kd_prodi'] = $value['kd_prodi'];
                $x['nmprodi'] = $value['prodi']['nmprodi'];
                $x['nmjen'] = $value['jenjang']['nmjen'];
                $x['kd_fak'] = $value['prodi']['fakultas']['kd_fak'];
                $x['nmfak'] = $value['prodi']['fakultas']['nmfak'];
                array_push($xres['data'], $x);
            }
        } else {
            $data = Mminat::with('jenjang', 'prodi.fakultas')->orderBy('kd_prodi', 'ASC')->orderBy('kd_jen', 'ASC')->get();
            foreach ($data as $key => $value) {
                $x['kd_minat'] = $value['kd_minat'];
                $x['nmminat'] = $value['nmminat'];
                $x['kd_jen'] = $value['kd_jen'];
                $x['kd_prodi'] = $value['kd_prodi'];
                $x['nmprodi'] = $value['prodi']['nmprodi'];
                $x['nmjen'] = $value['jenjang']['nmjen'];
                $x['kd_fak'] = $value['prodi']['fakultas']['kd_fak'];
                $x['nmfak'] = $value['prodi']['fakultas']['nmfak'];
                array_push($xres['data'], $x);
            }
        }
        $enkrip = new Enkrip();
        $xres['status'] = "0000";
        $res['data'] = $enkrip->enkrip(json_encode($xres));

        return response($res);
    }
    public function allprodi(Request $request)
    {
        $xres['data'] = [];
        $xs = 0;
        if ($request->input('kd_jen') != null) {
            $prodi = Prodi::with('Jenjang', 'fakultas')->where('kd_jen', $request->input('kd_jen'))->get();
        } elseif ($request->input('kd_fak') != null) {
            $prodi = Prodi::with('Jenjang', 'fakultas')->where('kd_fak', $request->input('kd_fak'))->get();
        } else {
            $prodi = Prodi::with('Jenjang', 'fakultas')->get();
        }
        foreach ($prodi as $key => $value) {
            // $x['kd_minat'] =  $value['kd_minat'];
            // $x['nmminat'] =  $value['nmminat'];
            $x['kd_jen'] = $value['kd_jen'];
            $x['kd_prodi'] = $value['kd_prodi'];
            $x['nmprodi'] = $value['nmprodi'];
            $x['nmjen'] = $value['jenjang']['nmjen'];
            $x['kd_fak'] = $value['fakultas']['kd_fak'];
            $x['nmfak'] = $value['fakultas']['nmfak'];
            array_push($xres['data'], $x);
            $xs = 1;
        }
        if ($xs == 1) {
            $xres['status'] = "0001";
        } elseif ($xs == 0) {
            $xres['status'] = "0002";
        }
        return response($xres);
    }

    public function resendemail(Request $request)
    {
        $enkrip = new Enkrip();
        $fungsi = new Fungsi();
        $passpl = $fungsi->generatepass();
        $pass = app('hash')->make($passpl);
        $calonmhs = Calonmhs::with('minat')->where('notest', $request->input('notest'))->where('email', $request->input('email'))->first();
        
        if ($calonmhs) {
            $calonmhs->password = $pass;
            $calonmhs->save();
            $arraymail = [
                'notest' => $request->input('notest'),
                'nama' => $calonmhs['nama'],
                'nmprodi' => "TI",
                'nmfakultas' => "Fakultas Teknologi Informasi",
                'nmjenjang' => "Strata 1",
                'email' => $request->input('email'),
                'nmjnsdftr' => 'Jalur Umum',
                'passpl' => $passpl];
            $mail = $fungsi->sendemail($arraymail);
            if($mail==false){
                $res['status'] = "0001";
            }else{
                $res['status'] = "0000";
            }
        } else {
            $res['status'] = '0002';
        }
        $xres['data'] = $enkrip->enkrip(json_encode($res));
        return response($xres);

    }
    public function sma(Request $request){
        $enkrip = new Enkrip();
        if($request->has('nama')){
            $data = Msma::where('nmsma','like','%'.$request->input('nama').'%')->get();
        }else{
            $data = Msma::all();
        }
        if($data){
            $xres['data'] = [];
            foreach ($data as $key => $value) {
                $x['kd_sma']= $value['kd_sma'];
                $x['nmsma']= $value['nmsma'];
                $x['alamat']= $value['alamat'];
                $x['kd_pos']= $value['kd_pos'];
                array_push($xres['data'],$x);
            }
            $xres['status']="0000";
        }else{
            $xres['status']="0002";
        }
        $res['data'] = $enkrip->enkrip(json_encode($xres));
        
        return response($res);

    }
    public function grade(Request $request, $id)
    {

        // ==============================
        $enkrip = new Enkrip();
        $calonmhs = Calonmhs::where('notest', $id)->first();
        $program = Prodi::with('fakultas')->where('kd_prodi', $calonmhs['kd_prodi'])->where('kd_jen', $calonmhs['kd_jen'])->first();
        // return response($program);
        if ($calonmhs) {
            if ($calonmhs['grade'] == "A") {
                $pesan = "Bebas 100% Biaya Kuliah";
                $gstatus = 'Lulus';

            } elseif ($calonmhs['grade'] == "B") {
                $pesan = "Bebas BIP";
                $gstatus = 'Lulus';

            } elseif ($calonmhs['grade'] == "C") {
                $pesan = "Potongan BIP 80%";
                $gstatus = 'Lulus';

            } elseif ($calonmhs['grade'] == "D") {
                $pesan = "Potongan BIP 50%";
                $gstatus = 'Lulus';

            }elseif ($calonmhs['grade'] == null) {
                $pesan = "Potongan BIP 50%";
                $gstatus = 'Belum Mendapatkan Grade';
            } else {
                $gstatus = 'Tidak Lulus';
                $pesan = "-";
            }

            $res['status'] = '0000';

            $res['notest'] = $calonmhs['notest'];
            $res['nama'] = $calonmhs['nama'];
            $res['alamat'] = $calonmhs['alamat'];
            $res['grade'] = $calonmhs['grade'];
            $res['nmprodi'] = $program['nmprodi'];
            $res['nmfak'] = $program['fakultas']['nmfak'];
            $res['pesan'] = $pesan;
            $res['gstatus'] = $gstatus;
            $res['daftarawal'] = "Senin, 16 Desember 2019";
            $res['daftarakhir'] = "Senin, 20 Desember 2019";
            $res['bayarakhir'] = "Senin, 27 Desember 2019";
            $res['batch'] = "2";
        } else {
            $res['status'] = "0002";
        }
        $xres['data'] = $enkrip->enkrip(json_encode($res));
        return response($xres);
    }
}
