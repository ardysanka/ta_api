<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Jenjang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenjang',function (Blueprint $table){
            $table->string('kd_jen',2);
            $table->primary('kd_jen');
            $table->string('nmjen',50);
        });
        Schema::create('prodi',function (Blueprint $table){
            $table->string('kd_prodi',2);
            $table->primary(array('kd_prodi', 'kd_jen'));
            $table->string('nmprodi',50);
            $table->string('kd_jen',2);
            $table->string('kd_fak',2);
            $table->string('nip',6);
            $table->char('status',6);
        });
        Schema::create('fakultas',function (Blueprint $table){
            
            $table->string('kd_fak',2);
            $table->primary('kd_fak');
            $table->string('nmfak',50);
            $table->string('singkat',25);
            $table->string('dekan',50);
            
        });
        Schema::create('minat',function (Blueprint $table){
            
            $table->string('kd_minat',2);
            $table->primary('kd_minat');
            $table->string('nmminat',50);
            $table->string('kd_prodi',2);
            $table->string('kd_jen',2);
            
            
            
        });
        Schema::create('sma',function (Blueprint $table){
            
            $table->string('kd_sma',2);
            $table->primary('kd_sma');
            $table->string('nmsma',50);
            $table->text('alamat')->nullable();;
            $table->string('kd_pos',6)->nullable();;
            
        });
        Schema::create('jadwaltest',function (Blueprint $table){
            
            $table->string('kd_test',6);
            $table->primary('kd_test');
            $table->date('tgltest');
            $table->string('ruang',20);
            $table->integer('maks')->unsigned();
            $table->integer('isi')->nullable()->unsigned();
            $table->string('gel',1);
            $table->string('thajar',8);
            
        });
        Schema::create('jadwaltest',function (Blueprint $table){
            
            $table->string('kd_test',6);
            $table->primary('kd_test');
            $table->date('tgltest');
            $table->string('ruang',20);
            $table->integer('maks')->unsigned();
            $table->integer('isi')->nullable()->unsigned();
            $table->string('gel',1);
            $table->string('thajar',8);
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
