<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalonmhsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calonmhs', function (Blueprint $table) {
            $table->string('notest',10);
            $table->primary('notest');
            $table->string('nama',40);
            $table->string('jenkel',1)->nullable();
            $table->text('alamat')->nullable();
            $table->string('telpon',15);
            $table->integer('nilaitest')->unsigned();
            $table->string('grade',1)->nullable();
            $table->string('user',8);
            $table->string('kd_sma',5)->nullable();
            $table->string('kd_test',6)->nullable();
            $table->string('kd_prodi',2);
            $table->string('kd_jen',2);
            $table->string('kd_minat',2);
            $table->string('thajar',8);
            $table->date('tgldftr');
            $table->string('thnlulus',4)->nullable();
            $table->string('email',60);
            $table->date('tgllhr')->nullable();
            $table->string('nmortu',15)->nullable();
            $table->string('telportu',15)->nullable();
            $table->string('agama',1)->nullable();
            $table->string('status',4);
            $table->string('password',32);
            $table->string('klsintern',1);
            $table->string('statuscalon',1);
            // $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calonmhs');
    }
}
