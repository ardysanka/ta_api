<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transbayar',function (Blueprint $table){
            
            $table->string('notrans',8);
            $table->primary('notrans');
            $table->string('notest',10);
            $table->date('tgltrans');
            $table->string('smt',1);
            $table->string('thajar',8);
            $table->string('ket')->nullable();
            $table->date('tglflag')->nullable();
            $table->string('jumbayar',12);
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
